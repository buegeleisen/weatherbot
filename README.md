# Java Weatherbot for Telegram: #
A small Telegram Bot that notifies the user about the weather.
Weather data from openweathermap.

### Future functions: 
* Scheduling of weather messages
* Weather forecasts
  
* Let user choose correct city if city exists multiple time
* limit users' saved cities to 50 to prevent attacks
