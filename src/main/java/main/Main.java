package main.java.main;


import main.java.bot.WeatherBot;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Calendar;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;


public class Main {

    // City ID Laudenbach: 2880204 , api key: d3f7462a1266a740672355292a9abb47
    //API Call: api.openweathermap.org/data/2.5/weather?id=2880204&appid=d3f7462a1266a740672355292a9abb47


    public static void main(String[] args) throws Exception{
        //create bot
        ApiContextInitializer.init();

        final TelegramBotsApi botsApi = new TelegramBotsApi();

        try {
            final WeatherBot wb = new WeatherBot();
            botsApi.registerBot(wb);
            /*Timer timer = new Timer("Weather");
            TimerTask tt = new TimerTask(){
                public void run(){

                    Calendar cal = Calendar.getInstance(); //this is the method you should use, not the Date(), because it is desperated.

                    int hour = cal.get(Calendar.HOUR_OF_DAY);//get the hour number of the day, from 0 to 23
                    int minute = cal.get(Calendar.MINUTE);

                    if(hour >= 7 && hour < 23 && minute == 30){
                        System.out.println("doing the scheduled task");
                        String messageString = WeatherBuilder.getWeather();
                        SendMessage message = new SendMessage() // Create a SendMessage object with mandatory fields
                                .setChatId("70177912")
                                .setText("This was a scheduled Task: "+messageString);


                        try {
                            wb.execute(message); // Call method to send the message
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            timer.schedule(tt, 1000, 1000*60);//	delay the task 1 second, and then run task every 60 seconds
            */



        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
