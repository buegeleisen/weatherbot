package main.java.main;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBConnector {

    public DBConnector(){

    }

    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:/home/buegeleisen/Projects/weatherbot/db/weatherbot.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public String getCityID(String city) {
        String sql = "SELECT id From cities_germany WHERE name='"+city+"' Limit 1;";
        String res = null;
        try {
            Connection conn = this.connect();
            Statement stmnt = conn.createStatement();
            ResultSet rs = stmnt.executeQuery(sql);
            res = rs.getString("id");
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return res;
    }

    public void saveCity(String user, String city){
        String sql = "INSERT INTO user_data (chatid,city) VALUES( '"+user+"',	'"+city+"')";
        try {
            Connection conn = this.connect();
            Statement stmnt = conn.createStatement();
            ResultSet rs = stmnt.executeQuery(sql);
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public List<String> getCities(String user){
        String sql = "SELECT DISTINCT city FROM user_data WHERE chatid='"+user+"'";
        List<String> cities = new ArrayList<String>();
        try {
            Connection conn = this.connect();
            Statement stmnt = conn.createStatement();
            ResultSet rs = stmnt.executeQuery(sql);

            while(rs.next()){
                String res = rs.getString("city");
                cities.add(res);
            }
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return cities;
    }

    public void deleteCities(String user) {
        String sql = "DELETE from user_data WHERE chatid='"+user+"'";
        try {
            Connection conn = this.connect();
            Statement stmnt = conn.createStatement();
            ResultSet rs = stmnt.executeQuery(sql);
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteCities(String user, String city) {
        String sql = "DELETE from user_data WHERE chatid='"+user+"' AND city='"+city+"'";
        try {
            Connection conn = this.connect();
            Statement stmnt = conn.createStatement();
            ResultSet rs = stmnt.executeQuery(sql);
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
