package main.java.main;

import com.google.gson.Gson;
import main.java.api.Weather;
import main.java.api.WeatherObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class WeatherBuilder {


    public static String getWeather(String cityId) {
        HttpURLConnection con = null;
        StringBuffer content = null;
        if (cityId != null) {
            try {
                URL url = new URL("http://api.openweathermap.org/data/2.5/weather?id=" + cityId + "&units=metric&appid=d3f7462a1266a740672355292a9abb47");
                System.out.println(url);
                con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();
                con.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String json = content.toString();
            System.out.println(json);

            Gson gson = new Gson();
            WeatherObject wo = gson.fromJson(json, WeatherObject.class);

            System.out.println("Weather in " + wo.getName() + " right now: " + wo.getMain().getTemp() + "°C and it is " + wo.getWeather().get(0).getDescription());

            return "Weather in " + wo.getName() + " right now: " + wo.getMain().getTemp() + "°C and it is " + wo.getWeather().get(0).getDescription();


        }
        return "";
    }


    /*
    public static String getWeather(String cityId) {
        WeatherObject wo = new WeatherObject();
        try{
            URL url = new URL("http://api.openweathermap.org/data/2.5/weather?id=" + cityId + "&units=metric&appid=d3f7462a1266a740672355292a9abb47");
            wo = getData(cityId, url);

        }catch(Exception e){
            e.printStackTrace();
        }
        return "Weather in " + wo.getName() + " right now: " + wo.getMain().getTemp() + "°C and it is " + wo.getWeather().get(0).getDescription();
    }

    public static String getForecast(String cityId) {
        WeatherObject wo = new WeatherObject();
        try{
            URL url = new URL("http://api.openweathermap.org/data/2.5/forecast?id=" + cityId + "&cnt=2&units=metric&appid=d3f7462a1266a740672355292a9abb47");
            wo = getData(cityId, url);

        }catch(Exception e){
            e.printStackTrace();
        }
        return "This is still a test" + wo.getName();
    }

    private static WeatherObject getData(String cityId, URL url){
        HttpURLConnection con = null;
        StringBuffer content = null;
        WeatherObject wo = new WeatherObject();
        if (cityId != null) {
            try {
                con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();
                con.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String json = content.toString();
            System.out.println(json);

            Gson gson = new Gson();
            wo = gson.fromJson(json, WeatherObject.class);

            return wo;

        }
        return wo;
    }
    */
}
