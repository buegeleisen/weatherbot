package main.java.bot;

import main.java.api.Sys;
import main.java.main.DBConnector;
import main.java.main.WeatherBuilder;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.*;

public class WeatherBot extends TelegramLongPollingBot {
    @Override
    public void onUpdateReceived(Update update) {
        // We check if the update has a message and the message has text
        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText();
            long chatId = update.getMessage().getChatId();
            System.out.println(chatId + " wrote: " + messageText);
            SendMessage message = new SendMessage();
            DBConnector db = new DBConnector();

            //Commands
            if(messageText.equals("/start") || messageText.equals("/help")){
                message = new SendMessage() // Create a SendMessage object with mandatory fields
                        .setChatId(chatId)
                        .setText("Welcome to the weather bot! \n\nThe Following commands are currently available:\n\n" +
                                "/get: Get the current Weather for a city specified by name. Example: /get Mannheim." +
                                "\n\n/save: Save a city to your personal list. Example: /save Mannheim." +
                                "\n\n/my: Get the weather report for all cities of your list." +
                                "\n\n/delete: Delete either all your saved cities or a specified one. Example: /delete" +
                                " Mannheim or just /delete to remove all.");
            }
            else if(messageText.equals("/list")){
                //TODO Get all cities
            }
            else if(messageText.startsWith("/get")){
                String city = messageText.substring(5);
                String cityID = db.getCityID(city);
                String answerString = WeatherBuilder.getWeather(cityID);


                message = new SendMessage() // Create a SendMessage object with mandatory fields
                        .setChatId(chatId)
                        .setText(answerString);

            }
            else if(messageText.startsWith("/save")){
                String city = messageText.substring(6);
                db.saveCity(chatId+"",city);
                message = new SendMessage() // Create a SendMessage object with mandatory fields
                        .setChatId(chatId)
                        .setText("City "+city+" has been stored!");
            }
            else if(messageText.equals("/my")){
                String user = chatId+"";
                List<String> cities = db.getCities(user);
                String answerString = "";

                for(int i = 0; i < cities.size(); i++){
                    String cityID = db.getCityID(cities.get(i));
                    answerString = answerString+" \n"+WeatherBuilder.getWeather(cityID);
                }
                if(answerString.equals("")){
                    answerString = "No cities!";
                }
                message = new SendMessage() // Create a SendMessage object with mandatory fields
                        .setChatId(chatId)
                        .setText(answerString);
            }
            else if(messageText.startsWith("/delete")){
                String text = "";
                if(messageText.length() == 7){
                    db.deleteCities(chatId+"");
                    text = "All cities have been removed!";
                }else{
                    String city = messageText.substring(8);
                    db.deleteCities(chatId+"", city);
                    text = city + " was removed!";
                }

                message = new SendMessage() // Create a SendMessage object with mandatory fields
                        .setChatId(chatId)
                        .setText(text);
            }
            else{
                message = new SendMessage().setChatId(chatId).setText("This command is unknown! Type /start or /help" +
                        " for more Information.");
            }

            try {
                execute(message); // Call method to send the message
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
      }

    @Override
    public String getBotUsername() {
        return "buegeleisenweatherbot";
    }

    @Override
    public String getBotToken() {
        return "1316335919:AAGntsL67BJQ-kBFt1g4DWcbSydjpA4FJYA";
    }


}
